package com.atb.controller;

import com.atb.model.AccountSummaryModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class AccountSummaryController {
    public static final Logger logger = LoggerFactory.getLogger(AccountSummaryController.class);

    @RequestMapping(value = "/acntsummary/", method = RequestMethod.GET)
    public ResponseEntity<List<AccountSummaryModel>> listAllUsers() {

        List<AccountSummaryModel> accounts = new ArrayList<>();
        return new ResponseEntity<List<AccountSummaryModel>>(accounts, HttpStatus.OK);
    }

}
