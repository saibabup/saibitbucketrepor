package com.atb;

import com.atb.controller.AccountSummaryController;
import com.atb.service.AccountSummarySvc;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.xml.soap.*;
import java.net.MalformedURLException;

@RunWith(SpringRunner.class)
@WebMvcTest(value = AccountSummaryController.class, secure = false)
public class EntapiApplicationTests {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private AccountSummarySvc accountSummaryService;

	private SOAPConnectionFactory soapConnectionFactory;
	private SOAPConnection soapConnection ;
	private MessageFactory messageFactory;
	private SOAPMessage soapMessage;
	private SOAPPart soapPart;
	private SOAPEnvelope soapEnvelope;
	private static String namespaceURI = "http://atb.com/xi/sap/bas/supportingbankingprocesses/bankaccountmanagement";
	private static String wsdlUrl = "http://localhost:6064/FindBankAccountContractByBusinessPartnerID.asmx?WSDL";
	private static String operationName = "FindBankAccountContractByBusinessPartnerID";
	private static String serviceName = "Service__-ATB_-BANKACCOUNTCONTRACTPROC14_DEFAULT_PROFILE";
	private static String portName = "Binding_T_HTTP_A_HTTP__-ATB_-BANKACCOUNTCONTRACTPROC14_DEFAULT_PROFILE";


	@Before
	public void setUp(){

		try {
			soapConnectionFactory = SOAPConnectionFactory.newInstance();
			soapConnection = soapConnectionFactory.createConnection();

			messageFactory = MessageFactory.newInstance();
			soapMessage = messageFactory.createMessage();

			soapPart = soapMessage.getSOAPPart();
			soapEnvelope = soapPart.getEnvelope();

		} catch (UnsupportedOperationException | SOAPException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void accountSummaryTest() throws MalformedURLException {

	}

}
